---
layout: post
title:  "Informática Jurídica - Workshop Técnico"
date: "2019-08-11T22:22:22+08:00"

summary: "Link y comentarios."
tags : ["informatica", "juridica"]
categories:
- informatica
- juridica
- internet
comments: true
author: 3manuek
---


## Workshop de componentes de Internet (UB)

Parte de una colaboración _ad honorem_, se dictó un Worshop de Internet
en la UB (Universidad de Belgrano, cátedra de Informática Jurídica a cargo de la Dra. Laura Brestolli), el cual está bajo licencia GPLv3. Se encuentra **WIP** (Work in Progress),
por lo que pueden surgir cambios:

[WIP Presentación](https://docs.google.com/presentation/d/1kh9USWjacvQTd7065ARuOFHwtldbc7zQKNkJwiLdhdI/edit?usp=sharing)


