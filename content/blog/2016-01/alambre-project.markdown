---
title: "Alambre, ugly scripts to save time of your day"
date: "2016-01-23T22:10:48+08:00"
tags: ["scripts", "projects"]
# image: images/alambre.jpg # https://koppl.in/indigo/assets/images/jekyll-logo-light-solid.png
draft: false

summary: "This is just ugly scripts intended to save time in daily basis."
jemoji: '<img class="emoji" title=":wrench:" alt=":wrench:" src="https://assets-cdn.github.com/images/icons/emoji/unicode/1f527.png" height="20" width="20" align="absmiddle">'
author: 3manuek

# date: "2018-01-30T15:04:48+08:00"
draft: false

# series: ["imageSeries"]
categories: ["project", "scripts"]
toc: true

---


As a DBA we usually have to do dirty things under a running clock. Using that moto, I started a repository
to start collecting those nasty things that save time a day. That's what [alambre](https://github.com/ayresdata/alambre) does: small and complex things.


---

What has inside?

- bash   (preferred)
- python
- SQL    

---

If you want to indicate issues, go for it at [report issues](https://github.com/ayresdata/alambre/issues).
