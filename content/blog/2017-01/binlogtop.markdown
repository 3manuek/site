---
title: "binlogtop"

date: "2017-01-23T22:22:00+08:00"

tags: ["scripts","project"]
# image: image/alambre.jpg # https://koppl.in/indigo/assets/images/jekyll-logo-light-solid.png
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
summary: "Top-like replication monitoring tool for MySQL."
toc: true
draft: false
categories: ["project","scripts"]
author: 3manuek
externalLink: false
---

Currently `binlogtop` is in status *WIP* and very alpha state, although there is a python
script doing sort of the same thing is: [binlogEventStats](https://github.com/3manuek/binlogEventStats).



- Hey! Replication is delaying, don't know what's happening.

[binlogtop](https://github.com/3manuek/binlogTop) just do a real-time streaming statistics.


---

What has inside?

- golang

---

If you want to indicate issues, go for it at [report issues](https://github.com/3manuek/binlogTop/issues).
