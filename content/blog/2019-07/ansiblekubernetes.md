---
layout: post
title:  "Ansible and Kubernetes"
subtitle: "."

date: "2019-07-18T22:22:00+08:00"

summary: "."
tags : [Ansible, Kubernetes]
categories:
- Ansible
- Kubernetes
draft: true
# comments: true
author: 3manuek
---

## Install minikube or similar


[](https://github.com/kubernetes/minikube/releases)

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.2.0/minikube-darwin-amd64 && chmod +x minikube && sudo cp minikube /usr/local/bin/ && rm minikube
```


## Getting the token

[Access Kubernetes API](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-api/)


## Ansible k8s 


[](https://docs.ansible.com/ansible/latest/modules/k8s_module.html#k8s-raw-module)

`K8S_AUTH_API_KEY` in environment.

## Getting facts with k8s_fact

[](https://docs.ansible.com/ansible/latest/modules/k8s_facts_module.html#k8s-facts-module)