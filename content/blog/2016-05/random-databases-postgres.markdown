---

title: "Random data for PostgreSQL"
date: "2016-05-01T22:10:48+08:00"
tags : ["script", "projects", "PostgreSQL"]
draft: false
summary: "Need some good synthetic data for testing?."
comments: true
author: 3manuek 
draft: false
# series: ["imageSeries"]
categories: ["PostgreSQL", "RDS"]
toc: true

---

Usually, you want to test code, try new features or just need decent content when
generating random data. [Random databases for Postgres][1] addresses this need by
simple SQL scripts that can be entirely generated inside your database.

It isn't a sophisticated repository, although it is practical for quick usage.

---

What has inside?

- Pure SQL

---

If you want to indicate issues, go for it at [report issues][2].

[1]: https://github.com/3manuek/Random-database-for-Postgres
[2]: https://github.com/3manuek/Random-database-for-Postgres/issues
