+++
date = "2018-09-02"
draft = false
title = "About"

+++

## Introduction

Hi fellow visitor! I am Emanuel Calvo, a DataBase Reliability Engineer (DBRE) and large-scale DevOps Engineer.

Mastered the _Art of doing changes in production cool as a cucumber_, probably I can be considered at least a Senior shit or something -- it might be one of the reasons they offer me money for.

I design reliable, scalable and ready-for-production Database infrastructures for some _top-noch_ companies, based on Open Source Technologies from the ground up.
That is, from coding the provisioning and deploy, components integrated in a Continuous Deployment (with Canary support, eg), to highly tweaked environments for performance and complex setups.

Other things that I love to do:

- I'm an experienced hydroponics _micro-grower_; DWC and NFT are my favorites and I do have both implemented.
- I'm an amateur musician, the typical string set plus some keyboards. Drawing is also a hobby.
- Love reading, particularly around polithical, econocomics and social affairs. An active Libertarian working from his basement :)

---

For professional contact, you can send a contact request or message through my
[LinkedIn Profile](https://www.linkedin.com/in/ecbcbcb) or email me at 3manuek [at] 3manuek [dot] com.

[GitHub](https://github.com/3manuek) and [Gitlab](https://gitlab.com/3manuek) profiles.

> As a previous and unrelated experience, I worked as Officer and had quit as Adjutor Principal after 8 years of serving at Argentina's Federal Bureau of Prisons (Servicio Penitenciario Federal). I had been in charge of assigned groups at several institutions from medium to high security. I cooperated in the design of some of the security and emergency procedures and designed the operations and personnel capacitation. In the early days of my career, on the External Security Officer role, I managed the weapon capacity and storage of the shifts. I also conducted sanction procedures and crime investigations in several of those establishments. I must highlight that it is not possible to go any further whether you want to change things for good; there will be always someone on top of you trying to keep his/her power. And that was one of the reasons why I quit.

## External publications

July 2nd, 2013: [AWS - Postgres in AWS](http://media.amazonwebservices.com/AWS_RDBMS_PostgreSQL.pdf)
    PostgreSQL running over EC2 instances with EBS/PIOPS storage. Minimum architecture, tips and advises.

[Download AWS in Postgres from local](/AWS_RDBMS_PostgreSQL.pdf)


## Certifications

[PostgreSQL 9.4 Associate Certification](http://www.enterprisedb.com/store/products/dba-training/01t50000001Nyi7AAC), `EDB33986`.

[PostgreSQL 11 Professional Certification](https://www.youracclaim.com/badges/9ea58cfa-2da5-4614-9d5e-2eb1be46dd95/linked_in_profile)


DataScience Specialization, Coursera (80% completed):

- [Toolbox](https://www.coursera.org/account/accomplishments/certificate/6UUQRK9PCE)
- [R programming](https://www.coursera.org/account/accomplishments/certificate/T48RRJNPG2)
- [Getting and Cleaning Data](https://www.coursera.org/account/accomplishments/verify/8AT8J4DK2A)
- [Exploratory Analysis](https://www.coursera.org/account/accomplishments/verify/PJSGHN7A27)
- [Reproducible Research](https://www.coursera.org/account/accomplishments/verify/J5VEMAG8TM)
- [Statistical inference](https://www.coursera.org/account/accomplishments/verify/K59SPFAKW2)
- [Regression Models](https://www.coursera.org/account/accomplishments/verify/DLH75DKK4U)

[Mongo M102 - MongoDB for DBAs (Completed Training Course and Exams - Score 100%)](http://university.mongodb.com/course_completion/2c40dd69b1894e63a80542bb9ce0aeb2)

[Big Data, University of San Diego](https://www.coursera.org/account/accomplishments/verify/TFTDEL6ZYX97)

## Education

- ESBA - Developer Analyst

---

## Current positions

### Ongres Inc. (ex 8kdata) - Database Engineer
> Remote. New York/Madrid
> Since September 2017 as OnGres, previously working as Consultant in 8kdata

At OnGres, I'm working on Architecture, Reliabiliy and Infrastructure of our customers' production Databases.
In that list, we can name a companies like  **Gitlab**, **BBM (Blackberry)**, **Conrad Electronics**, **ARM** and **Postmates**
I practice a wide approach on all the Data Stack component's aspects, such as Performance, Scalability and Reliability.

Highlighted projects:

- BBM (Blackberry - Emtek - Google Cloud): Live migration from Oracle to full Patroni automated cluster, from a premise
  based environment to Google Cloud. PgBouncer-Consul-Postgres
- Conrad (Google Cloud Partner): Full implementation of Patroni HA using etcd as consensus cluster, Load Balancing through 
  Google iLB with API configuration with Patroni, and PgBouncer.
- GitLab as part of the Database Reliability Team and Database Architecture design.
- Postmates: Patroni Mult-project Terraform implementation
- Postgres vs MongoDB `10k U$S` automated benchmark, IaaC (Hashicorp echosystem, AWS provider based).
- Google Cloud as Partner.
- ARM

Techs:

- Infra and CD/CI: HCL (Terraform, Packer), Ansible, Kubernetes, Docker.
- Databases: Postgres (and extensions), MySQL, Oracle (to migrate out *wink, wink*)
- Languages: Python, Golang, bash
- Main Cloud Providers: Google Cloud, AWS

### Smok Media -- IT Platform Director
> El Rio de la Plata, Argentina
> October, 2019 - Current

As part of the Smok Media group, we develop public face sites for start ups and entrepeneurs.
Managing resources in the cloud and setting up the platform for general usage, such as streaming,
chat platform, Social Network scrapping and customer follow up.



### Ayres.io  -- Co-Working Spaces
> Palermo, Argentina
> September, 2016 - Current

Ayres.io is a coworking project for hosting companies at one of the most cool towns down here in 
South America for Start-Ups.


---

## Past Experience

### Percona -  Senior Technical Services Engineer _[main position]_
> Buenos Aires, Argentina
> August 2016 - September 2017

Full description of the role [here](https://www.percona.com/about-percona/careers/senior-technical-services-engineer).

I held the Remote DBA position at Team Atlas.

Some highlighted customers: Hyperwallet, Fitbit, Lookout, Shipwire

### 8kData - Data External Advisor
> Madrid, Spain
> January 2016 - September 2017

External technical advisor and consultant for Postgres and other related technologies.

### UDE - Open Source Source Undergraduate Degree Associate Professor
> La Plata, Argentina
> June 2016

Open Source Source Undergraduate Degree Associate Professor, PostgreSQL Database specialist.


### Pythian - Technical Lead - ACTG (Advanced Consulting Team Group) Member
> Ottawa, Canada
> January 2015 - June 2016

- Support to Pythian’s External Profile.
- Member of the Advanced Consulting Team Group (PostgreSQL).
- Other Technologies: MySQL/MariaDB, xtraDB, Galera, Docker, Ansible, MongoDB, PostgreSQL, Vertica, Oracle.
- Part of the ACTG for PostgreSQL implementations, solutions and architecture.
- Lead Database Consultant T73

Some highlighted customers: Microsoft, Beats (Apple), Adobe Echosign, Fitbit, Sendgrid, Mozilla Foundation, Zendesk, SendGrid.

### 2ndQuadrant - PostgreSQL Consultant
> Buenos Aires, Argentina
> February 2014 - January 2015

- PostgreSQL consultant, support and training.
- Testing new features, debugging and code review.


### BlackBird IT (ex-PalominoDB) - Database Operations - Professional Services
> Las Vegas, NV
> September 2011 - February 2014

- Primary DBA/Teach Lead of the PostgresXC databases.
- MySQL-and-forks/PostgreSQL-and-Forks administration over several customers.
- Considerable amount of experience running DDL/Query/Capacity/Server Reviews.
- Technical writer (site is under "pythian.com"): http://www.palominodb.com/category/postgres
- Backups, Replication, HA, Monitoring, Trending, Health Reviews, DDL Reviews, percona toolkits and several Postgres tools.
- Some other databases that I'm currently working with: MongoDB, Cassandra, Redis.
- AWS experience. EC2, RDS, S3, LB, EBS, Redshift, DynamoDB, AWS API.
- Open Stack Cloud experience.
- Extensive experience on server reviews, workload tracing and SQL rewriting and tuning.
- Scripting/development: Python, Perl , PL/pgsql and bash. In reverse order.

Some of the highlighted customers: Adobe Echosign, Fitbit, Zendesk, Chegg, SendGrid

### Aedgency - MySQL/Oracle DBA
> Barcelona, Spain
> November 2010 - March 2012

- MySQL/Oracle database administration.
- Query tuning and performance.
- ETL integration between databases.
- PL/SQL development.
- Bash scripting.
- Sphinx basic configuration, fixing  and monitoring.

### SIU - DBA & Database Production Consulting
> Buenos Aires, Argentina
> July 2009 - July 2010

- Provided traning and support to national universities (as UNLP, UNC, UNNE, UNNOBA, UNCU, UNPA, UNR, etc), 
support developers in DB modeling and administration of Postgresql-Informix-Mysql databases.
- Data Architect tasks and server revisions. Planning migrations and future strategies to board new releases.
- Coordinated teams for database support to enhance the compliant between apps of SIU.
- Developing the production and pre-prod following plan for all the SIU apps. Server metrics and other stuff for high performance servers.

### Globant - SysAdmin Linux - EA
> Buenos Aires, Argentina
> February 2009 -  July 2009

- SysAdmin Unix at EA (Electronic Arts) Hydra - GOP project.
- Maintained platform consisted in Oracle database 10, Apaches and Tomcats (VAP, VCM) Clustered, with several stages of deploy.
- Main tasks were relate to deploys, environment configuration, VMWare Infraestructure Client maintenance and monitoring.
- I did Mysql DBA tasks on Prod, Pre-Production and Development Servers for a Forums project. Fixing some character set problems and DDL, backups, restores, check, performance and others.
- Basic knowledge on deployment / process with Maven - Ant - Hudson.
- Assigned 50% to another project to provide Engineering Support at EA over Python scripting.

### Correo Argentino - SysAdmin senior Linux and PostgreSQL
> Monte Grande, Argentina
> August 2008  -  February 2009

- I managed production platform at Correo Argentino (over 400 terms with local databases and Linux enviroment).
- Some machines was enlaced with a AD throw WinBind.
- Created an improved deploy tool for scripts, database recovery, control and monitoring, etc.
- Also managed servers over Centos 5 for the support area's knowledge-base application  (7 DB MySQL 5.1)

### Siemens - PHP developer
> Monte Grande, Argentina
> January - August 2008

Admitel Platform developer. Consists in  PHP + (Javascript, Ajax, x/Html, xml) - Oracle 8i.
Some of the 'extra' task was made some Perl scripts for support area.


### Huenei - Application Support
> Monte Grande , Argentina
> October 2007 - January 2008


- Terminal administration in W/NT and 2K remote servers. Configuration, control, monitoring. Attend Remedy tickets for fix problems and others.
- The application was in Paradox and Oracle databases, in which my tasks consisted in repair tables, fix some wrong data and others.
- Bug reporting and detection.
- MOSAIC debugging and support.
- Customer Correo Argentino / Siemens.

----

## Customers History

As a consultant, I provided independent database consulting services for the following companies (in non-particular order).
Thanks to every customer that trusted in me!

- iMedicare (USA)
- Eloquentix (USA)
- Ericsson/EDB (USA)
- Pic-pic (USA)
- PFA (Argentina)
- Autofarma (Arg)
- VSTour (Arg)
- Jampp (Arg/USA)
- Cyclelogic (Arg)
- Avalon Informática (Madrid)
- Init (País Vasco)
- Scienza (Argentina)
- Syscope (Argentina)
- Mexane (Argentina)
- Contenta Mobile (Argentina)
- Slots Machines (San Luis, Argentina)
- TreeLogic (Asturias)
- BTS (Usa - Zaragoza)
- LCRCOM.net (Madrid)
- Pampa Business Solutions - Telecentro (Argentina)
- Sindicato de Pasteleros y afines (Arg)
- 8kdata (Spain, Madrid)
