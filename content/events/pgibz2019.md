+++
date = "2019-07-01T22:22:22-03:00"
draft = false
title = "PgIbz 2019"
categories = ["events"]

+++

## PgIbz 2019

<!-- {{% center %}}<img name="go2shell-finder" src="https://pgibz.io/assets/img/logo.png" width='250px'/>{{% /center %}} -->

{{% img-no-border %}}{{% center %}}<img name="Automator" src="https://pgibz.io/assets/img/logo.png" width='100px'/>{{%/ center %}}{{% /img-no-border %}}

This event has been held in Ibiza, at the Convention Center and it was organized by the PostgreSQL Foundation. This pure-PostgreSQL event hosted several talks, among I lectured "Pooling Performance".

The code used for this presentation will be public soon as I'm currently rewriting the benchPlatform into a more sophisticated tool.

Slides can be found [here](https://github.com/3manuek/slides/tree/master/2019/pgibz).
