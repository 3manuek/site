+++
date = "2015-08-19T20:29:37-07:00"
draft = false
title = "Events"

+++

In this section, I'll post about the conferences and events I had attended and presented a topic. Generally, the slides are at [my slide's repo](https://github.com/3manuek/slides).

I attended and lecture on a lot of conferences! I'll be uploading more information about all of them soon, just started from the latest.

Si te interesan las Bases de Datos, sumate a <a href="http://www.canaldba.com/"> CanalDBA </a> en <a href="https://join.slack.com/t/canaldba/shared_invite/enQtMzUyNjMxNzY3MTI3LTMwNzhjZDNkYjU2ZjliYjc1M2VhNTliNzQ4MDhlYWU2YTNjOTljOGYwOTU3NTIxMjdkZTcxMTY0Zjg3YmY4ODk">Slack</a>!.
