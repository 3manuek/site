
+++
date = "2018-10-01T22:22:22-03:00"
draft = false
title = "Nerdear.la 2018"
categories = ["events"]

+++

Great event to attend if you are around Buenos Aires in October: [Nerdear.la](https://nerdear.la/). It helds talks of a ranged variety of topics,
high-level to hardcore IT talks.

I lectured (in Spanish) about the _Relational Databases: the good, the bad and the ugly_

<div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/J3Aw2qJ-470?ecver=2" width="480" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
