+++
date = "2017-05-02T22:22:22-03:00"
draft = false
title = "Percona Live 2017"
categories = ["events"]

+++

## Percona Live 2017

{{% center %}}
<a href="https://www.percona.com/live/17/content/demystifying-postgres-logical-replication" align="middle"><img align="middle"  src="https://www.percona.com/sites/default/files/118x239_PLSpeaking_17.png"  alt="Percona Live 2017, Open Source Database Conference, April 24-27, 2017" title="MySQL, MongoDB, PostgreSQL, ODBMS" /></a>
{{%/ center %}}


Been held at _Santa Clara, CA_, I lectured here about one the most significant features in Postgres, Logical Replication:

<div style="position:relative;height:0;padding-bottom:75.0%"><iframe src="https://www.youtube.com/embed/zq3CHvLkfno?ecver=2" width="480" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
